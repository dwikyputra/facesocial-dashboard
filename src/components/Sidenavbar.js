import React from 'react'
import { Sidenav, Nav, Icon } from 'rsuite'
import logo from '../assets/logo.svg';

const Sidenavbar = (props) => {
  return ( 
    <Sidenav expanded={true} className="sidenav">
      <Sidenav.Header className="side-header">
        <img src={logo} className="app-logo" alt="logo" />
      </Sidenav.Header>
      <Sidenav.Body>
        <Nav>
          {props.routes.map((route, index) => (
            <Nav.Item key={index} eventKey={index} icon={<Icon icon={route.icon} />} href={route.path}>
              {route.text}
            </Nav.Item>
          ))}
        </Nav>
      </Sidenav.Body>
    </Sidenav>
  );
}
 
export default Sidenavbar;