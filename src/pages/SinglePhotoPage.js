import React from 'react'
import { Row, Col, Panel } from 'rsuite'

import API from "../utils/API"

class SinglePhotoPage extends React.Component {
  constructor(props) {
    super(props);
    this.photoId = props.match.params.photoId;
    this.albumId = props.match.params.albumId;
    this.name = props.match.params.name;
    this.state = {
      data: {}
    }
  }

  async componentDidMount(){
    let albumData, photoData
    try {
      albumData = await API.get('albums/'+this.albumId);
      photoData = await API.get('photos/'+this.photoId);
    } catch (e) {
      console.log(`😱 Axios request failed: ${e}`);
    }

    photoData = {
      name: this.name,
      albumTitle: albumData.data.title,
      photoId: photoData.data.id,
      title: photoData.data.title,
      url: photoData.data.url,
      thumbnailUrl: photoData.data.thumbnailUrl
    }
    this.setState({data:photoData})
  }


  render() {
    const { data } = this.state;
    return (
      <div>
        <h3 style={{marginBottom:20}}>Single Photo</h3>
        <Row className="show-grid">
          <Col xs={8}>
            <Panel header={"Photo of "+data.name} bordered style={{minHeight:500}}>
              <img width={300} src={data.url} alt="img"/>
              <h4 style={{marginTop:20}}>{data.title}</h4>
            </Panel>
          </Col>
        </Row>
      </div>
    );
  }
}

export default SinglePhotoPage