import React from 'react'
import { Table } from 'rsuite'

import API from "../utils/API"


class UserPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: []
    };
  }

  async componentDidMount(){
    let userData
    try {
      userData = await API.get('users');
    } catch (e) {
      console.log(`😱 Axios request failed: ${e}`);
    }
    userData = userData.data.map(ud=>{
      return {
        id: ud.id,
        name: ud.name,
        username: ud.username,
        email: ud.email,
        address: ud.address.street+", "+ud.address.suite+", "+ud.address.city,
        phone: ud.phone,
        website: ud.website,
        company: ud.company.name
      }
    })
    this.setState({data:userData})
  }

  render() {
    return (
      <div>
        <h3 style={{marginBottom:20}}>User</h3>
        <Table
          height={650}
          data={this.state.data}
        >
          <Table.Column width={70} align="center">
            <Table.HeaderCell>Id</Table.HeaderCell>
            <Table.Cell dataKey="id" />
          </Table.Column>

          <Table.Column width={180}>
            <Table.HeaderCell>Name</Table.HeaderCell>
            <Table.Cell dataKey="name" />
          </Table.Column>

          <Table.Column width={150}>
            <Table.HeaderCell>Username</Table.HeaderCell>
            <Table.Cell dataKey="username" />
          </Table.Column>

          <Table.Column width={190}>
            <Table.HeaderCell>Email</Table.HeaderCell>
            <Table.Cell dataKey="email" />
          </Table.Column>

          <Table.Column width={300}>
            <Table.HeaderCell>Address</Table.HeaderCell>
            <Table.Cell dataKey="address" />
          </Table.Column>

          <Table.Column width={180}>
            <Table.HeaderCell>Phone</Table.HeaderCell>
            <Table.Cell dataKey="phone" />
          </Table.Column>

          <Table.Column width={140}>
            <Table.HeaderCell>Website</Table.HeaderCell>
            <Table.Cell dataKey="website" />
          </Table.Column>

          <Table.Column width={170}>
            <Table.HeaderCell>Company</Table.HeaderCell>
            <Table.Cell dataKey="company" /> 
          </Table.Column>
        </Table>
      </div>
    );
  }
}

export default UserPage