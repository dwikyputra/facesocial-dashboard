import React from 'react'
import { Table, Row, Col, Panel, Button } from 'rsuite'
import { Link } from 'react-router-dom'

import API from "../utils/API"

class SinglePostPage extends React.Component {
  constructor(props) {
    super(props);
    this.postId = props.match.params.postId;
    this.name = props.match.params.name;
    this.state = {
      data: {}
    }
  }

  async componentDidMount(){
    let postData, commentData
    try {
      commentData = await API.get('comments?postId='+this.postId);
      postData = await API.get('posts/'+this.postId);
    } catch (e) {
      console.log(`😱 Axios request failed: ${e}`);
    }

    postData = {
      name: this.name,
      postId: postData.data.id,
      title: postData.data.title,
      body: postData.data.body,
      commentsCount: commentData.data.length,
      comments: commentData.data
    }
    this.setState({data:postData})
  }

  async deleteItem(endpoint, id) {
    let confirmDelete = window.confirm('Delete item forever?')
    if(confirmDelete){
      let success = true
      try {
        await API.delete(endpoint+'/'+id);
      } catch (e) {
        success = false
        console.log(`😱 Axios request failed: ${e}`);
      }
      if (success) {
        alert('Delete success')
        if(endpoint == 'posts') window.location.replace("/post");
        else window.location.reload();
      } else {
        alert('Delete failed')
      }
    }
  }  


  render() {
    const { data } = this.state;
    return (
      <div>
        <h3 style={{marginBottom:20}}>Single Post</h3>
        <Row className="show-grid">
          <Col xs={6}>
            <Panel header="Post" bordered style={{minHeight:400}}>
              <p style={{marginBottom:20}}>{data.name}</p>
              <h4>{data.title}</h4>
              <p style={{paddingTop:20,paddingBottom:30}}>{data.body}</p>
              <div style={{textAlign:'right'}}>
                <Button className="panel-button" color="cyan" appearance="primary">Edit</Button> {' '}
                <Button onClick={()=>this.deleteItem('posts', this.postId)} className="panel-button" color="red" appearance="ghost">Remove</Button>
              </div>
            </Panel>
          </Col>
          <Col xs={18}>
            <Panel header="Comments" bordered style={{minHeight:300}}>
              <Button className="panel-button" color="green" appearance="primary" style={{marginBottom:20}}>New Comment</Button>
              <p style={{marginBottom:20}}>Count: {data.commentsCount}</p>
              <Table
                height={500}
                data={data.comments}
                wordWrap
              >
                <Table.Column width={70} align="center">
                  <Table.HeaderCell>Id</Table.HeaderCell>
                  <Table.Cell dataKey="id" />
                </Table.Column>

                <Table.Column width={200}>
                  <Table.HeaderCell>Name</Table.HeaderCell>
                  <Table.Cell dataKey="name" />
                </Table.Column>

                <Table.Column width={200}>
                  <Table.HeaderCell>Email</Table.HeaderCell>
                  <Table.Cell dataKey="email" />
                </Table.Column>

                <Table.Column width={400}>
                  <Table.HeaderCell>Body</Table.HeaderCell>
                  <Table.Cell dataKey="body" />
                </Table.Column>

                <Table.Column width={120} fixed="right">
                <Table.HeaderCell>Action</Table.HeaderCell>
                <Table.Cell>
                  {rowData => {
                    return (
                      <span>
                        <a> Edit </a>|{' '}
                        <a onClick={()=>this.deleteItem('comments', rowData.id)}> Remove </a>
                      </span>
                    );
                  }}
                </Table.Cell>
              </Table.Column>                
              </Table>
            </Panel>
          </Col>
        </Row>
      </div>
    );
  }
}

export default SinglePostPage