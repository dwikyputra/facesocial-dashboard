

import React from 'react';
import { Switch, Route } from 'react-router-dom'

import UserPage from './pages/UserPage'
import PostPage from './pages/PostPage'
import AlbumPage from './pages/AlbumPage'
import SinglePostPage from './pages/SinglePostPage'
import SingleAlbumPage from './pages/SingleAlbumPage'
import SinglePhotoPage from './pages/SinglePhotoPage'

const DashboardPage = () => <h1>Dashboard</h1>

export const SidebarRoutes = [
  {
    path: "/",
    icon: "dashboard",
    text: "Dashboard",
    component: DashboardPage
  },  
  {
    path: "/user",
    icon: "user",
    text: "User",
    component: <UserPage/>
  },
  {
    path: "/post",
    icon: "newspaper-o",
    text: "Post",
    component: <PostPage/>
  },
  {
    path: "/album",
    icon: "image",
    text: "Album",
    component: <AlbumPage/>
  }
];

const RouterContent = () => {
  return ( 
    <Switch>
      {SidebarRoutes.map((route) => (
        <Route key={route.path} path={route.path} exact >
          {route.component}
        </Route>
      ))}
      <Route path='/post/:name/:postId' exact component={SinglePostPage} />
      <Route path='/album/:name/:albumId' exact component={SingleAlbumPage} />
      <Route path='/photo/:name/:albumId/:photoId' exact component={SinglePhotoPage} />
    </Switch>
  );
}
 
export default RouterContent;
