import React from 'react'
import { Navbar, Nav, Avatar } from 'rsuite'

const Headernav = () => {
  return (
    <Navbar>
      <Navbar.Header> 
        <div className="nav-header">FaceSocial Dashboard</div>
      </Navbar.Header>
      <Navbar.Body>
        <Nav pullRight>
          <Nav.Item className="custom-avatar-wrapper">
            <Avatar
              circle
              src="https://avatars2.githubusercontent.com/u/12592949?s=460&v=4"
            />
            <span>Administrator</span>
          </Nav.Item> 
        </Nav>
      </Navbar.Body>
    </Navbar>
  );
};

export default Headernav;