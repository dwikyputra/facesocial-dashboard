import React from 'react';
import { BrowserRouter } from 'react-router-dom'
import { Container } from 'rsuite'
import Sidenavbar from './components/Sidenavbar'
import Headernav from './components/Headernav'
import RouterContent, { SidebarRoutes } from './Router'

import './assets/App.css';
import 'rsuite/dist/styles/rsuite-default.css';


function App() {
  return (
    <BrowserRouter>
      <Headernav/>
      <div className="content-wrapper">
        <Sidenavbar routes={SidebarRoutes}/>
        <Container className="container">
          <RouterContent/>
        </Container>
      </div>      
    </BrowserRouter>
  );
}

export default App;
