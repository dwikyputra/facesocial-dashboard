import React from 'react'
import { Table, Row, Col, Panel, Avatar } from 'rsuite'
import { Link } from 'react-router-dom'

import API from "../utils/API"

class SingleAlbumPage extends React.Component {
  constructor(props) {
    super(props);
    this.albumId = props.match.params.albumId;  
    this.name = props.match.params.name;
    this.state = {
      data: {}
    }
  }

  async componentDidMount(){
    let albumData, photoData
    try {
      photoData = await API.get('photos?albumId='+this.albumId);
      albumData = await API.get('albums/'+this.albumId);
    } catch (e) {
      console.log(`😱 Axios request failed: ${e}`);
    }

    albumData = {
      name: this.name,
      albumId: albumData.data.id,
      title: albumData.data.title,
      photosCount: photoData.data.length,
      photos: photoData.data
    }
    this.setState({data:albumData})
  }


  render() {
    const { data } = this.state;
    return (
      <div>
        <h3 style={{marginBottom:20}}>Single Album</h3>
        <Row className="show-grid">
          <Col xs={6}>
            <Panel header="Album" bordered style={{minHeight:200}}>
              <p style={{marginBottom:20}}>{data.name}</p>
              <h4>{data.title}</h4>
            </Panel>
          </Col>
          <Col xs={18}>
            <Panel header="Photos" bordered style={{minHeight:300}}>
              <p style={{marginBottom:20}}>Count: {data.photosCount}</p>
              <Table
                height={500}
                data={data.photos}
                wordWrap
              >
                <Table.Column width={70} align="center">
                  <Table.HeaderCell>Id</Table.HeaderCell>
                  <Table.Cell dataKey="id" />
                </Table.Column>

                <Table.Column width={400}>
                  <Table.HeaderCell>Title</Table.HeaderCell>
                  <Table.Cell>
                    {rowData => {
                      return (
                        <Link to={"/photo/"+data.name+"/"+data.albumId+"/"+rowData.id}> {rowData.title} </Link>
                      );
                    }}
                  </Table.Cell>
                </Table.Column>

                <Table.Column width={300}>
                  <Table.HeaderCell>Url</Table.HeaderCell>
                  <Table.Cell dataKey="url" />
                </Table.Column>

                <Table.Column width={100}>
                  <Table.HeaderCell>Thumbnail</Table.HeaderCell>
                  <Table.Cell>
                    {rowData => {
                      return (
                        <Avatar circle src={rowData.thumbnailUrl} />
                      );
                    }}
                  </Table.Cell>
                </Table.Column>             
              </Table>
            </Panel>
          </Col>
        </Row>
      </div>
    );
  }
}

export default SingleAlbumPage