import React from 'react'
import { Table, Button } from 'rsuite'
import { Link } from 'react-router-dom'

import API from "../utils/API"

class PostPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: []
    }
  }

  async componentDidMount(){
    let postData, userData
    try {
      userData = await API.get('users');
      postData = await API.get('posts');
    } catch (e) {
      console.log(`😱 Axios request failed: ${e}`);
    }

    postData = userData.data.map(u=>{
      var posts = postData.data.filter(p => p.userId === u.id)
      return {
        id: u.id,
        name: u.name,
        postCount: posts.length,
        posts: posts
      }
    })
    this.setState({data:postData})
  }


  render() {
    const { data } = this.state;
    return (
      <div>
        <h3 style={{marginBottom:20}}>Post</h3>
        <Button className="panel-button" color="green" appearance="primary" style={{marginBottom:20}}>New Post</Button>
        <Table
          height={650}
          data={data}
          wordWrap
        >
          <Table.Column width={70} align="center">
            <Table.HeaderCell>Id</Table.HeaderCell>
            <Table.Cell dataKey="id" />
          </Table.Column>

          <Table.Column width={200}>
            <Table.HeaderCell>Name</Table.HeaderCell>
            <Table.Cell dataKey="name" />
          </Table.Column>

          <Table.Column width={150}>
            <Table.HeaderCell>Post Count</Table.HeaderCell>
            <Table.Cell dataKey="postCount" />
          </Table.Column>

          <Table.Column width={550} fixed="right">
            <Table.HeaderCell>Post Titles</Table.HeaderCell>
            <Table.Cell>
              {rowData => {
                return (
                  <ul>
                    {
                      rowData.posts.map((po, i) => (
                        <li key={i}>
                          <Link to={"/post/"+rowData.name+"/"+po.id}> {po.title} </Link>
                        </li>
                      ))
                    }
                  </ul>
                );
              }}
            </Table.Cell>
          </Table.Column>
        </Table>
      </div>
    );
  }
}

export default PostPage