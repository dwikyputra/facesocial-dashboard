import React from 'react'
import { Table } from 'rsuite'
import { Link } from 'react-router-dom'

import API from "../utils/API"

class AlbumPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: []
    }
  }

  async componentDidMount(){
    let albumData, userData
    try {
      userData = await API.get('users');
      albumData = await API.get('albums');
    } catch (e) {
      console.log(`😱 Axios request failed: ${e}`);
    }

    albumData = userData.data.map(u=>{
      var albums = albumData.data.filter(p => p.userId === u.id)
      return {
        id: u.id,
        name: u.name,
        albumCount: albums.length,
        albums: albums
      }
    })
    this.setState({data:albumData})
  }


  render() {
    const { data } = this.state;
    return (
      <div>
        <h3 style={{marginBottom:20}}>Album</h3>
        <Table
          height={650}
          data={data}
          wordWrap
        >
          <Table.Column width={70} align="center">
            <Table.HeaderCell>Id</Table.HeaderCell>
            <Table.Cell dataKey="id" />
          </Table.Column>

          <Table.Column width={200}>
            <Table.HeaderCell>Name</Table.HeaderCell>
            <Table.Cell dataKey="name" />
          </Table.Column>

          <Table.Column width={150}>
            <Table.HeaderCell>Album Count</Table.HeaderCell>
            <Table.Cell dataKey="albumCount" />
          </Table.Column>

          <Table.Column width={550} fixed="right">
            <Table.HeaderCell>Album Titles</Table.HeaderCell>
            <Table.Cell>
              {rowData => {
                return (
                  <ul>
                    {
                      rowData.albums.map((al, i) => (
                        <li key={i}>
                          <Link to={"/album/"+rowData.name+"/"+al.id}> {al.title} </Link>
                        </li>
                      ))
                    }
                  </ul>
                );
              }}
            </Table.Cell>
          </Table.Column>
        </Table>
      </div>
    );
  }
}

export default AlbumPage